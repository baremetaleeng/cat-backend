from sqlalchemy import Column, Integer, String, Boolean, SmallInteger, DateTime, Numeric, ForeignKey, Float
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Gateway(Base):
    __tablename__ = 'gateways'

    id = Column(Integer, primary_key=True)
    port = Column(Integer) # 1 for status 2 for GPS
    status_id = Column(Integer, ForeignKey('status.id'))
    position_id = Column(Integer, ForeignKey('positions.id'))
    device_id = Column(Integer, ForeignKey('devices.id'))
    gateway_id = Column(String(20))
    rssi = Column(Integer)
    snr = Column(Float)

    status = relationship("Status")
    position = relationship("Position")

    def __init__(self, port, gateway_id, rssi, snr, device, status=None, position=None):
        self.port = port
        self.gateway_id = gateway_id
        self.rssi = rssi
        self.snr = snr
        self.device = device
        self.status = status
        self.position = position


class Position(Base):
    __tablename__ = 'positions'

    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    device_id = Column(Integer, ForeignKey('devices.id'))
    longitude = Column(Numeric(10 ,7))
    latitude = Column(Numeric(10 ,7))
    altitude = Column(Numeric(5,1))
    hdop = Column(Numeric(3,1))
    satellites = Column(SmallInteger)
    telegram_number = Column(Integer)

    device = relationship("Device")

    def __init__(self, date, device, longitude, latitude, altitude, hdop, satellites, telegram_number):
        self.date = date
        self.device = device
        self.longitude = longitude
        self.latitude = latitude
        self.altitude = altitude
        self.hdop = hdop
        self.satellites = satellites
        self.telegram_number = telegram_number

    def __repr__(self):
        return "<Position(id=%d, lat='%s', lng='%s', alt='%s')>" % (self.id, self.latitude, self.longitude, self.altitude)

    def to_dict(self):
        return {
            'id': self.id,
            'date': self.date,
            'device_id': self.device_id,
            'longitude': str(self.longitude),
            'latitude': str(self.latitude),
            'altitude': str(self.altitude),
            'hdop': str(self.hdop),
            'satellites': self.satellites,
            'telegram_number': self.telegram_number
        }

class Status(Base):
    __tablename__ = "status"

    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    device_id = Column(Integer, ForeignKey('devices.id'))
    voltage = Column(Integer)

    device = relationship("Device")

    def __init__(self, date, device, voltage):
        self.date = date
        self.device = device
        self.voltage = voltage

    def __repr__(self):
        return "<Status(id=%d, dev_id='%s', voltage='%d'" % (self.id, self.device.dev_id, self.voltage)

    def to_dict(self):
        return {
            'id': self.id,
            'date': self.date,
            'device_id': self.device_id,
            'voltage': self.voltage
        }

class Device(Base):
    __tablename__ = 'devices'

    id = Column(Integer, primary_key=True)
    number = Column(String(16), unique=True)
    dev_id = Column(String(16), unique=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    name = Column(String(255))
    tracking = Column(Boolean)
    gps_perm = Column(Boolean)
    signal = Column(Boolean)
    charging = Column(Boolean)
    firmware_version = Column(SmallInteger)
    min_hdop = Column(Integer)
    send_frequency = Column(Integer)
    downlink_url = Column(String(255))

    user = relationship("User", back_populates="devices")

    def __init__(self, number, dev_id, user=None, name=None):
        self.number = number
        self.dev_id = dev_id
        if user:
            self.user = user
        if name:
            self.name = name

    def __repr__(self):
        return "<Device(id=%d, dev_id='%s', user_id='%s', name='%s', tracking='%s', gps_perm='%s', firmware_version='%s')>" % (self.id, self.dev_id, self.user_id, self.name, self.tracking, self.gps_perm, self.firmware_version)

    def to_dict(self):
        return {
            'id': self.id,
            'dev_id': self.dev_id,
            'user_id': self.user_id,
            'name': self.name,
            'tracking': self.tracking,
            'gps_perm': self.gps_perm,
            'firmware_version': self.firmware_version,
            'downlink_url': self.downlink_url
        }

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(255), unique=True)
    password = Column(String(255))
    email = Column(String(255))
    session_id = Column(String(255))

    devices = relationship("Device", order_by=Device.id, back_populates="user")

    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email

    def __repr__(self):
        return "<User(id=%d, username='%s', password='%s', email='%s')>" % (self.id, self.username, self.password, self.email)

    def to_dict(self):
        return {
            'id': self.id,
            'username': self.username,
            'password': self.password,
            'email': self.email
        }
