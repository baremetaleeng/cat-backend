from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine(
    'mysql+pymysql://cat:cat@localhost/catracker?charset=utf8',
    connect_args = {
        'port': 3306
    },
    echo='debug',
    echo_pool=True,
    pool_timeout=1
)

db_session = scoped_session(
    sessionmaker(
        bind=engine,
        autocommit=False,
        autoflush=False
    )
)

Base = declarative_base()

def init_db():
    import database.models
    Base.metadata.create_all(engine)
