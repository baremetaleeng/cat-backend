from database.models import User, Device, Status, Position, Gateway
from api import db, helper

def insert_status(data):
    device = db.session.query(Device).filter(Device.dev_id == data["dev_id"]).first()
    # no device found so insert new one
    if not device:
        device = Device(data["hardware_serial"], data["dev_id"])
        db.session.add(device)
        db.session.commit()
    send_status = False
    if device.gps_perm != data["payload_fields"]["gps_perm_on"]:
        device.gps_perm = data["payload_fields"]["gps_perm_on"]
        send_status = True

    if device.tracking != data["payload_fields"]["track"]:
        device.tracking = data["payload_fields"]["track"]
        send_status = True

    device.charging = data["payload_fields"]["charging"]

    if device.signal != data["payload_fields"]["signal"]:
        device.signal = data["payload_fields"]["signal"]
        send_status = True

    device.downlink_url = data["downlink_url"]
    device.firmware_version = data["payload_fields"]["version"]
    status = Status(data["time"], device, data["payload_fields"]["bat"])
    db.session.add(status)
    db.session.add(device)
    db.session.commit()
    db.session.remove()
    if send_status:
        helper.send_status_to_device(device)

def insert_position(data):
    device = db.session.query(Device).filter(Device.dev_id == data["dev_id"]).first()
    if device:
        position = Position(data["time"], device, 
            data["payload_fields"]["longitude"],
            data["payload_fields"]["latitude"],
            data["payload_fields"]["altitude"],
            data["payload_fields"]["hdop"],
            data["payload_fields"]["sats"],
            data["counter"]
            )
        db.session.add(position)
        db.session.commit()
    db.session.remove()
