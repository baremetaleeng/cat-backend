from api import app, db, helper
from flask import jsonify, request, abort, make_response, escape, session
from datetime import datetime
from database.models import User, Device, Status, Position, Gateway
import json
from passlib.hash import sha256_crypt
import base64

import config
from . import ingress

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found', 'string': str(error)}), 404)

@app.route('/api/v1.1', methods=['GET'])
def info():
    return jsonify({'status': 'Version 1.1'})

@app.route('/api/v1.1/ingress', methods=['POST'])
def add_pos_11():
    if not request.json or not 'dev_id' in request.json:
        abort(400)
    if request.headers.get("Authorization") != config.DATA_INGRESS_KEY:
        abort(403)
    pos = request.json
    pos["time"] = datetime.strptime(pos["metadata"]["time"][:19], "%Y-%m-%dT%H:%M:%S")
    if pos["port"] == 1: #status
        ingress.insert_status(pos)
    elif pos["port"] == 2: #position
        ingress.insert_position(pos)
    
    return make_response(jsonify({'status': 'OK'}), 201)

@app.route('/api/v1.1/pos/<string:sessionKey>/<string:dev_id>')
def serve_dev_pos(sessionKey, dev_id):
    user = db.session.query(User).filter(User.session_id == sessionKey).first()
    if not user:
        return make_response("", 401)
    device = db.session.query(Device).filter(Device.dev_id == dev_id).first()
    if request.args.get("from"):
        starttime = datetime.strptime(request.args.get("from"), "%Y%m%d%H%M%S")
        if starttime:
            if request.args.get("to"):
                endtime = datetime.strptime(request.args.get("to"), "%Y%m%d%H%M%S")
            else:
                endtime = datetime.now()
        positions = db.session.query(Position).filter(Position.device_id == device.id, Position.date.between(starttime, endtime)).order_by(Position.date.desc()).all()
        pos = []
        for p in positions:
            pos.append(p.to_dict())
        return make_response(jsonify({'pos': pos}), 200)
    #nur aktuellste position ausgeben
    position = db.session.query(Position).filter(Position.device_id == device.id).order_by(Position.date.desc()).first()
    if position: 
        pos = [position.to_dict()]
    else:
        pos = []
    return make_response(jsonify({'pos': pos}), 200)

@app.route('/api/v1.1/status/<string:sessionKey>/<string:dev_id>', methods=['GET'])
def serve_dev_status(sessionKey, dev_id):
    user = db.session.query(User).filter(User.session_id == sessionKey).first()
    if not user:
        return make_response("", 401)
    device = db.session.query(Device).filter(Device.dev_id == dev_id).first()
    status = db.session.query(Status).filter(Status.device_id == device.id).order_by(Status.date.desc()).first()
   
    stat = {
        'battery': status.to_dict(),
        'signal': device.signal,
        'charging': device.charging,
        'firmware_version': device.firmware_version,
        'gps_perm': device.gps_perm,
        'tracking': device.tracking,
        'min_hdop': device.min_hdop,
        'send_frequency': device.send_frequency
    }
    return make_response(jsonify(stat), 200)

@app.route('/api/v1.1/status/<string:sessionKey>/<string:dev_id>', methods=['POST'])
def set_dev_status(sessionKey, dev_id):
    user = db.session.query(User).filter(User.session_id == sessionKey).first()
    if not user:
        return make_response("", 401)
    device = db.session.query(Device).filter(Device.dev_id == dev_id).first()
    if not device:
        return make_response("device not found", 404)
    hdop = float(request.form.get("hdop"))
    device.min_hdop = hdop * 10
    device.send_frequency = int(request.form.get("send_frequency"))
    device.tracking = True if request.form.get("track") == "true" else False
    device.gps_perm = True if request.form.get("perm_on") == "true" else False
    device.signal = True if request.form.get("signal") == "true" else False
    helper.send_status_to_device(device)
    db.session.add(device)
    db.session.commit()
    return make_response("", 201)

@app.route('/api/v1.1/device/<string:sessionKey>', methods=['GET'])
def get_devices(sessionKey):
    user = db.session.query(User).filter(User.session_id == sessionKey).first()
    if not user:
        return make_response("", 401)
    devices = []
    for d in user.devices:
        devices.append(d.to_dict())
    return make_response(jsonify({"devices": devices}), 200)
   
@app.route('/api/v1.1/device/<string:sessionKey>', methods=['POST'])
def add_device(sessionKey):
    user = db.session.query(User).filter(User.session_id == sessionKey).first()
    if not user:
        return make_response("", 401)
    dev = db.session.query(Device).filter(Device.number == request.form.get("dev_number"),
        Device.dev_id == request.form.get("dev_id")).first()
    if dev and dev.user:
        return make_response(jsonify({"status": "Device in use"}), 401)
    if not dev:
        dev = Device(
            dev_id=request.form.get("dev_id"),
            number=request.form.get("dev_number"),
            name=request.form.get("name"),
            user=user
        )
    else:
        dev.name=request.form.get("name"),
        dev.user=user
    db.session.add(dev)
    db.session.commit()
    return make_response(jsonify({"status": "OK"}), 200)


@app.route('/api/v1.1/login', methods=['POST'])
def user_login():
    if request.form["user"]: # untreated user imput!!!
        user = db.session.query(User).filter(User.username == (request.form["user"])).first()
        if user:
            if sha256_crypt.verify(request.form["password"], user.password):
                user.session_id = base64.b64decode(sha256_crypt.hash(user.username)).hex()
                db.session.commit()
                return make_response({"sessionKey": user.session_id}, 200)
    return make_response(jsonify({'error': "login failed"}), 401)

@app.route('/api/v1.1/register', methods=["POST"])
def user_register():
    if request.form["user"] and request.form["password"] and request.form["email"]:
        if db.session.query(User).filter(User.username == request.form["user"]).count():
            return make_response("", 409)
        user = User( # untreated user input!!!!!
            username=(request.form["user"]),
            password=sha256_crypt.hash(request.form["password"]),
            email=(request.form["email"])
        )
        db.session.add(user)
        db.session.commit()
        return make_response("", 201)
    return make_response("", 409)
