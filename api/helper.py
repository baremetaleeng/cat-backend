import requests
import json
from api import db

def send_status_to_device(dev):
    payload = {
    "dev_id": dev.dev_id,
    "port": 1,
    "confirmed": True,
    "payload_fields": {
        "track": dev.tracking,
        "gps_perm_on": dev.gps_perm,
        "signal": dev.signal,
        "min_hdop": dev.min_hdop,
        "send_frequency": dev.send_frequency
    },
    "schedule": "replace"
    }
    print(requests.post(dev.downlink_url, data=json.dumps(payload)))