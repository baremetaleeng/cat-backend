# ToDo für API

* [X] User Funktionen
    - [X] User anlegen
    - [X] User anmelden
    - [ ] Tracker registrieren
    - [ ] GET User Daten (Name, Liste der Tracker)
* [ ] Positionsdaten (nur nach Anmeldung)
    - [ ] GET Anzahl der Datensätze
    - [X] GET Range an Datensätzen
    - [X] Datenmengen Bereinigen
* [ ] Statusdaten (nur nach Anmeldung)
    - [X] GET Aktuelle Batterie Ladung + Schaltzustände (GPS, Tracking, HDOP, DC)
    - [X] POST Schaltzustände
    - [ ] GET Historischen Batteriedaten (Zeitraum)
