from database.database import db_session, init_db
from database.models import User, Device, Status, Position
from datetime import datetime

p = Position(datetime.now(), 1, 49.0, 8.0, 10.0, 2.0, 5, 1)
db_session.add(p)
db_session.commit()

quit()

from pymongo import MongoClient

mongo = MongoClient("mongodb://localhost:27017/catracker")

user = db_session.query(User).filter(User.username == "cat").first()

status = []
for device in user.devices:
    print("   --->  ", device.dev_id)
    for entry in mongo.catracker.pos.find({"dev_id": device.dev_id}).sort([("time", -1)]).limit(10000):
        dt = datetime.strptime(entry.get("time"), "%Y-%m-%dT%H:%M:%SZ")
        payload = entry.get("payload_fields")
        status.append(Status(dt, device, payload.get("bat")))
    print("   <--- ", len(status))

db_session.add_all(status)
db_session.commit()

"""

print("Devices: ", user.devices)
print("User: ", user)
"""
"""
db_session.add_all([
    Device("3633323563386C17", "proto2"),
    Device("3633323557386817", "proto3"),
    Device("363332357C386C17", "proto1otaa"),
])
db_session.commit()
"""



